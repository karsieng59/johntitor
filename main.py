# [START gae_python37_app]
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Hello john90-titor'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
# [END gae_python37_app]